## CSC320 Winter 2017 
## Assignment 2
## (c) Kyros Kutulakos
##
## DISTRIBUTION OF THIS CODE ANY FORM (ELECTRONIC OR OTHERWISE,
## AS-IS, MODIFIED OR IN PART), WITHOUT PRIOR WRITTEN AUTHORIZATION 
## BY THE INSTRUCTOR IS STRICTLY PROHIBITED. VIOLATION OF THIS 
## POLICY WILL BE CONSIDERED AN ACT OF ACADEMIC DISHONESTY

##
## DO NOT MODIFY THIS FILE ANYWHERE EXCEPT WHERE INDICATED
##

import numpy as np
import cv2 as cv

# File psi.py define the psi class. You will need to 
# take a close look at the methods provided in this class
# as they will be needed for your implementation
import psi        

# File copyutils.py contains a set of utility functions
# for copying into an array the image pixels contained in
# a patch. These utilities may make your code a lot simpler
# to write, without having to loop over individual image pixels, etc.
import copyutils

#########################################
## PLACE YOUR CODE BETWEEN THESE LINES ##
#########################################

# If you need to import any additional packages
# place them here. Note that the reference 
# implementation does not use any such packages

#########################################
import scipy.ndimage
from scipy import signal
#########################################
#
# Computing the Patch Confidence C(p)
#
# Input arguments: 
#    psiHatP: 
#         A member of the PSI class that defines the
#         patch. See file inpainting/psi.py for details
#         on the various methods this class contains.
#         In particular, the class provides a method for
#         accessing the coordinates of the patch center, etc
#    filledImage:
#         An OpenCV image of type uint8 that contains a value of 255
#         for every pixel in image I whose color is known (ie. either
#         a pixel that was not masked initially or a pixel that has
#         already been inpainted), and 0 for all other pixels
#    confidenceImage:
#         An OpenCV image of type uint8 that contains a confidence 
#         value for every pixel in image I whose color is already known.
#         Instead of storing confidences as floats in the range [0,1], 
#         you should assume confidences are represented as variables of type 
#         uint8, taking values between 0 and 255.
#
# Return value:
#         A scalar containing the confidence computed for the patch center
#

def computeC(psiHatP=None, filledImage=None, confidenceImage=None):
    assert confidenceImage is not None
    assert filledImage is not None
    assert psiHatP is not None
    
    #########################################
    ## PLACE YOUR CODE BETWEEN THESE LINES ##
    #########################################
    
    # Replace this dummy value with your own code
    M, N = filledImage.shape
    x_start = max(0, psiHatP._coords[0] - psiHatP._w / 2)
    x_end = min(M, psiHatP._coords[0] + psiHatP._w / 2 + 1)
    y_start = max(0, psiHatP._coords[1] - psiHatP._w / 2)
    y_end = min(N, psiHatP._coords[1] + psiHatP._w / 2 + 1)
    # print 'current pixel: '
    # print psiHatP._coords
    # print x_start
    # print x_end
    # print y_start
    # print y_end 
    C = np.sum(1 / 255.0 * filledImage[x_start:x_end, y_start:y_end] * confidenceImage[x_start:x_end, y_start:y_end]) / ((y_end - y_start) * (x_end - x_start))
    # print '\n'
    # print 'C is :'
    # print C
    #########################################
    return C

#########################################
#
# Computing the max Gradient of a patch on the fill front
#
# Input arguments: 
#    psiHatP: 
#         A member of the PSI class that defines the
#         patch. See file inpainting/psi.py for details
#         on the various methods this class contains.
#         In particular, the class provides a method for
#         accessing the coordinates of the patch center, etc
#    filledImage:
#         An OpenCV image of type uint8 that contains a value of 255
#         for every pixel in image I whose color is known (ie. either
#         a pixel that was not masked initially or a pixel that has
#         already been inpainted), and 0 for all other pixels
#    inpaintedImage:
#         A color OpenCV image of type uint8 that contains the 
#         image I, ie. the image being inpainted
#
# Return values:
#         Dy: The component of the gradient that lies along the 
#             y axis (ie. the vertical axis).
#         Dx: The component of the gradient that lies along the 
#             x axis (ie. the horizontal axis).
#
    
def computeGradient(psiHatP=None, inpaintedImage=None, filledImage=None):
    assert inpaintedImage is not None
    assert filledImage is not None
    assert psiHatP is not None
    
    #########################################
    ## PLACE YOUR CODE BETWEEN THESE LINES ##
    #########################################
    
    # Replace these dummy values with your own code
    # Dy = 1.0
    # Dx = 0.0

    M, N = filledImage.shape
    x_start = max(0, psiHatP._coords[0] - psiHatP._w / 2)
    x_end = min(M, psiHatP._coords[0] + psiHatP._w / 2 + 1)
    y_start = max(0, psiHatP._coords[1] - psiHatP._w / 2)
    y_end = min(N, psiHatP._coords[1] + psiHatP._w / 2 + 1)

    current = np.sum(inpaintedImage[x_start:x_end, y_start:y_end], axis = -1) / 3
    # print '==================='
    # print current
    try:
        valid_temp = (signal.convolve2d(filledImage[x_start-1:x_end+1, y_start-1:y_end+1], np.array([[1,1,1],[1,1,1],[1,1,1]]), mode="valid")>=255*9).astype(int)
        all_dx = scipy.ndimage.sobel(current, 1)
        all_dy = scipy.ndimage.sobel(current, 0)
        valid = np.zeros((all_dx.shape[0], all_dx.shape[1]))
        valid[0:valid_temp.shape[0], 0:valid.shape[1]] = valid_temp
        # print filledImage[x_start-1:x_end+1, y_start-1:y_end+1]
        # print valid
        # print valid_temp
    except IndexError:
        return 0.0, 0.0
    # print all_dx 
    # print all_dy
    all_dx *= valid
    all_dy *= valid
    mag = all_dx * all_dx + all_dy * all_dy
    # print mag
    max_idx = np.unravel_index(mag.argmax(), mag.shape)
    # print max_idx
    Dx = all_dx[max_idx]
    Dy = all_dy[max_idx]
    # length = np.sqrt(Dy * Dy + Dx * Dx)
    # if length == 0:
    #     return 0.0, 0.0
    # Dy /= length
    # Dx /= length
    # print 'Dx'
    # print Dx
    # print 'Dy'
    # print Dy
    # print '\n'
    # print Dx
    # print Dy
    #########################################
    return Dy, Dx

#########################################
#
# Computing the normal to the fill front at the patch center
#
# Input arguments: 
#    psiHatP: 
#         A member of the PSI class that defines the
#         patch. See file inpainting/psi.py for details
#         on the various methods this class contains.
#         In particular, the class provides a method for
#         accessing the coordinates of the patch center, etc
#    filledImage:
#         An OpenCV image of type uint8 that contains a value of 255
#         for every pixel in image I whose color is known (ie. either
#         a pixel that was not masked initially or a pixel that has
#         already been inpainted), and 0 for all other pixels
#    fillFront:
#         An OpenCV image of type uint8 that whose intensity is 255
#         for all pixels that are currently on the fill front and 0 
#         at all other pixels
#
# Return values:
#         Ny: The component of the normal that lies along the 
#             y axis (ie. the vertical axis).
#         Nx: The component of the normal that lies along the 
#             x axis (ie. the horizontal axis).
#
# Note: if the fill front consists of exactly one pixel (ie. the
#       pixel at the patch center), the fill front is degenerate
#       and has no well-defined normal. In that case, you should
#       set Nx=None and Ny=None
#

def computeNormal(psiHatP=None, filledImage=None, fillFront=None):
    assert filledImage is not None
    assert fillFront is not None
    assert psiHatP is not None

    #########################################
    ## PLACE YOUR CODE BETWEEN THESE LINES ##
    #########################################
    
    # Replace these dummy values with your own code
    Ny = 0.0
    Nx = 1.0
    M, N = filledImage.shape
    x_start = max(0, psiHatP._coords[0] - 2)
    x_end = min(M, psiHatP._coords[0] + 3)
    y_start = max(0, psiHatP._coords[1] - 2)
    y_end = min(N, psiHatP._coords[1] + 3)
    if np.sum(fillFront/ 255.0) <= 1:
        return None, None
    if fillFront[psiHatP._coords[0], psiHatP._coords[1]] <= 1:
        return None, None
    current = np.zeros((5,5))
    temp = filledImage[x_start:x_end, y_start:y_end]
    current[0:temp.shape[0], 0:temp.shape[1]] = temp
    # all_dx = scipy.ndimage.sobel(current, 0)
    # all_dy = scipy.ndimage.sobel(current, 1)
    # print current
    # print all_dx
    # print all_dy
    sobel_x = np.array([[1, 2, 0, -2, -1], [4, 8, 0, -8, 4], [6, 12, 0, -12, -6], [4, 8, 0, -8, -4], [1, 2, 0, -2, -1]])
    sobel_y = np.array([[1, 4, 6, 4, 1], [2, 8, 12, 8, 2], [0, 0, 0, 0, 0], [-2, -8, -12, -8, -2], [-1, -4, -6, -4, -1]])
    Ny = np.sum(sobel_y * current)
    Nx = np.sum(sobel_x * current)
    length = np.sqrt(Ny * Ny + Nx * Nx)
    if length == 0:
        return 0.0, 0.0
    Ny /= length
    Nx /= length
    # print 'Ny'
    # print Ny
    # print 'Nx'
    # print Nx
    # (vx, vy, x0, y0) = cv.fitLine(np.transpose(np.nonzero(fillFront[x_start:x_end, y_start:y_end])), cv.DIST_L2, 0, 0.01, 0.01)
    # Ny = -vx[0]
    # Nx = vy[0]
    # print vx
    # print vy
    # print x0
    # print y0
    #########################################

    return Ny, Nx