## CSC320 Winter 2017 
## Assignment 1
## (c) Kyros Kutulakos
##
## DISTRIBUTION OF THIS CODE ANY FORM (ELECTRONIC OR OTHERWISE,
## AS-IS, MODIFIED OR IN PART), WITHOUT PRIOR WRITTEN AUTHORIZATION 
## BY THE INSTRUCTOR IS STRICTLY PROHIBITED. VIOLATION OF THIS 
## POLICY WILL BE CONSIDERED AN ACT OF ACADEMIC DISHONESTY

##
## DO NOT MODIFY THIS FILE ANYWHERE EXCEPT WHERE INDICATED
##

# import basic packages
import numpy as np
import scipy.linalg as sp
import cv2 as cv

# If you wish to import any additional modules
# or define other utility functions, 
# include them here

#########################################
## PLACE YOUR CODE BETWEEN THESE LINES ##
#########################################


#########################################

#
# The Matting Class
#
# This class contains all methods required for implementing 
# triangulation matting and image compositing. Description of
# the individual methods is given below.
#
# To run triangulation matting you must create an instance
# of this class. See function run() in file run.py for an
# example of how it is called
#
class Matting:
    #
    # The class constructor
    #
    # When called, it creates a private dictionary object that acts as a container
    # for all input and all output images of the triangulation matting and compositing 
    # algorithms. These images are initialized to None and populated/accessed by 
    # calling the the readImage(), writeImage(), useTriangulationResults() methods.
    # See function run() in run.py for examples of their usage.
    #
    def __init__(self):
        self._images = { 
            'backA': None, 
            'backB': None, 
            'compA': None, 
            'compB': None, 
            'colOut': None,
            'alphaOut': None, 
            'backIn': None, 
            'colIn': None, 
            'alphaIn': None, 
            'compOut': None, 
        }

    # Return a dictionary containing the input arguments of the
    # triangulation matting algorithm, along with a brief explanation
    # and a default filename (or None)
    # This dictionary is used to create the command-line arguments
    # required by the algorithm. See the parseArguments() function
    # run.py for examples of its usage
    def mattingInput(self): 
        return {
            'backA':{'msg':'Image filename for Background A Color','default':None},
            'backB':{'msg':'Image filename for Background B Color','default':None},
            'compA':{'msg':'Image filename for Composite A Color','default':None},
            'compB':{'msg':'Image filename for Composite B Color','default':None},
        }
    # Same as above, but for the output arguments
    def mattingOutput(self): 
        return {
            'colOut':{'msg':'Image filename for Object Color','default':['color.tif']},
            'alphaOut':{'msg':'Image filename for Object Alpha','default':['alpha.tif']}
        }
    def compositingInput(self):
        return {
            'colIn':{'msg':'Image filename for Object Color','default':None},
            'alphaIn':{'msg':'Image filename for Object Alpha','default':None},
            'backIn':{'msg':'Image filename for Background Color','default':None},
        }
    def compositingOutput(self):
        return {
            'compOut':{'msg':'Image filename for Composite Color','default':['comp.tif']},
        }
    
    # Copy the output of the triangulation matting algorithm (i.e., the 
    # object Color and object Alpha images) to the images holding the input
    # to the compositing algorithm. This way we can do compositing right after
    # triangulation matting without having to save the object Color and object
    # Alpha images to disk. This routine is NOT used for partA of the assignment.
    def useTriangulationResults(self):
        if (self._images['colOut'] is not None) and (self._images['alphaOut'] is not None):
            self._images['colIn'] = self._images['colOut'].copy()
            self._images['alphaIn'] = self._images['alphaOut'].copy()

    # If you wish to create additional methods for the 
    # Matting class, include them here

    #########################################
    ## PLACE YOUR CODE BETWEEN THESE LINES ##
    #########################################

    #########################################
            
    # Use OpenCV to read an image from a file and copy its contents to the 
    # matting instance's private dictionary object. The key 
    # specifies the image variable and should be one of the
    # strings in lines 54-63. See run() in run.py for examples
    #
    # The routine should return True if it succeeded. If it did not, it should
    # leave the matting instance's dictionary entry unaffected and return
    # False, along with an error message
    def readImage(self, fileName, key):
        success = False
        msg = 'Placeholder'

        #########################################
        ## PLACE YOUR CODE BETWEEN THESE LINES ##
        #########################################
        img = cv.imread(fileName)
        if img is None:
            return False, 'read ' + key + ' image failed'
        self._images[key] = img.astype('float')
        success = True
        msg = 'read ' + key + ' image succeeded'
        #########################################
        return success, msg

    # Use OpenCV to write to a file an image that is contained in the 
    # instance's private dictionary. The key specifies the which image
    # should be written and should be one of the strings in lines 54-63. 
    # See run() in run.py for usage examples
    #
    # The routine should return True if it succeeded. If it did not, it should
    # return False, along with an error message
    def writeImage(self, fileName, key):
        success = False
        msg = 'Placeholder'                 

        #########################################
        ## PLACE YOUR CODE BETWEEN THESE LINES ##
        #########################################

        try:
            success = cv.imwrite(fileName, self._images[key])
        except Exception:
            print 
            return False, 'write ' + key + ' image failed'
        success = True
        msg = 'write ' + key + ' image succeeded'
        #########################################
        return success, msg

    # Method implementing the triangulation matting algorithm. The
    # method takes its inputs/outputs from the method's private dictionary 
    # ojbect. 
    def triangulationMatting(self):
        """
success, errorMessage = triangulationMatting(self)
        
        Perform triangulation matting. Returns True if successful (ie.
        all inputs and outputs are valid) and False if not. When success=False
        an explanatory error message should be returned.
        """

        success = False
        msg = 'Placeholder'

        #########################################
        ## PLACE YOUR CODE BETWEEN THESE LINES ##
        #########################################
        if self._images['backA'] is None:
            return False, 'backA is None'
        if self._images['backB'] is None:
            return False, 'backB is None'
        if self._images['compA'] is None:
            return False, 'compA is None'
        if self._images['compB'] is None:
            return False, 'compB is None'

        # ### lagacy
        # M1, M2, M3 = self._images['backA'].shape
        # b = np.concatenate((self._images['compA'] - self._images['backA'], self._images['compB'] - self._images['backB']), axis=-1).reshape(M1, M2, M3 * 2, 1)
        # # print b[0][0]
        # # print self._images['backA'].shape
        # # print self._images['backA'].reshape((M1, M2, 1, M3)).shape
        # c = np.tile(np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), (M1, M2, 1, 1))
        # # print c.shape
        # # print self._images['backA'].shape
        # AA = np.concatenate((c,-self._images['backA'].reshape((M1, M2, M3, 1))), axis=-1)
        # AB = np.concatenate((c,-self._images['backB'].reshape((M1, M2, M3, 1))), axis=-1)
        # A = np.concatenate((AA, AB), axis=-2)
        # # print A.shape
        
        # # done generating A and b in fomular Ax = b
        # A_inverse = np.array([map(np.linalg.pinv, x) for x in A])
        # # print A_inverse.shape
        # # print A_inverse[0][0]
        # # print A_inverse.shape
        # # print b.shape
        # x = np.zeros((M1, M2, 4, 1))
        # # print x.shape
        # # x = np.dot(A_inverse, b)
        # for i in range(M1):
        #     for j in range(M2):
        #         x[i][j] = np.dot(A_inverse[i][j], b[i][j])
        # # print x.shape
        # x = x.reshape(M1, M2, 4)
        # # print x[:,150:160,:3]
        # # print x[:,150:160,3]
        # self._images['colOut'] = x[:,:,:3]
        # self._images['alphaOut'] = x[:,:,3] * 255
        # print self._images['colOut']
        # print self._images['alphaOut']

        #### paper method
        # #BGR
        M1, M2, M3 = self._images['backA'].shape
        # a = self._images['compA'][:,:,0] - self._images['compB'][:,:,0]
        # b = self._images['backA'][:,:,0] - self._images['backB'][:,:,0]

        bf1 = self._images['compA'][:,:,0]
        gf1 = self._images['compA'][:,:,1]
        rf1 = self._images['compA'][:,:,2]

        bf2 = self._images['compB'][:,:,0]
        gf2 = self._images['compB'][:,:,1]
        rf2 = self._images['compB'][:,:,2]
        
        bk1 = self._images['backA'][:,:,0]
        gk1 = self._images['backA'][:,:,1]
        rk1 = self._images['backA'][:,:,2]
        
        bk2 = self._images['backB'][:,:,0]
        gk2 = self._images['backB'][:,:,1]
        rk2 = self._images['backB'][:,:,2]

        # print self._images['compA'].shape
        # self._images['alphaOut'] = (1 - np.divide(a, b, out=np.zeros_like(a), where=abs(b)<1e5))
        self._images['alphaOut'] = 255 * (1 - ((rf1-rf2)*(rk1-rk2)+(gf1-gf2)*(gk1-gk2)+(bf1-bf2)*(bk1-bk2)) / ((rk1-rk2)*(rk1-rk2) + (gk1-gk2)*(gk1-gk2) + (bk1-bk2)*(bk1-bk2)))
        self._images['colOut'] = (self._images['compA'] + self._images['compB']) * self._images['alphaOut'].reshape(M1, M2, 1) / 510
    
        # print self._images['colOut'].shape
        # print self._images['alphaOut']
        success = True
        msg = "triangulationMatting succeeded"
        #########################################

        return success, msg

        
    def createComposite(self):
        """
success, errorMessage = createComposite(self)
        
        Perform compositing. Returns True if successful (ie.
        all inputs and outputs are valid) and False if not. When success=False
        an explanatory error message should be returned.
"""

        success = False
        msg = 'Placeholder'

        #########################################
        ## PLACE YOUR CODE BETWEEN THESE LINES ##
        #########################################
        if self._images['colIn'] is None:
            return False, 'loading colIn failed'
        if self._images['alphaIn'] is None:
            return False, 'loading alphaIn failed'
        if self._images['backIn'] is None:
            return False, 'loading backIn failed'
        try:
            self._images['compOut'] = self._images['colIn'] + (255.0 - self._images['alphaIn'])/255.0 * self._images['backIn']
        except Exception:
            return False, "createComposite failed"
        success = True
        msg = "createComposite succeeded"
        #########################################

        return success, msg
     


