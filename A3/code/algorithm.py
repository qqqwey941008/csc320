# CSC320 Winter 2017
# Assignment 3
# (c) Olga (Ge Ya) Xu, Kyros Kutulakos
#
# DISTRIBUTION OF THIS CODE ANY FORM (ELECTRONIC OR OTHERWISE,
# AS-IS, MODIFIED OR IN PART), WITHOUT PRIOR WRITTEN AUTHORIZATION
# BY KYROS KUTULAKOS IS STRICTLY PROHIBITED. VIOLATION OF THIS
# POLICY WILL BE CONSIDERED AN ACT OF ACADEMIC DISHONESTY

#
# DO NOT MODIFY THIS FILE ANYWHERE EXCEPT WHERE INDICATED
#

# import basic packages
import numpy as np

# basic numpy configuration

# set random seed
np.random.seed(seed=131)
# ignore division by zero warning
np.seterr(divide='ignore', invalid='ignore')


# This function implements the basic loop of the PatchMatch
# algorithm, as explained in Section 3.2 of the paper.
# The function takes an NNF f as input, performs propagation and random search,
# and returns an updated NNF.
#
# The function takes several input arguments:
#     - source_patches:      The matrix holding the patches of the source image,
#                            as computed by the make_patch_matrix() function. For an
#                            NxM source image and patches of width P, the matrix has
#                            dimensions NxMxCx(P^2) where C is the number of color channels
#                            and P^2 is the total number of pixels in the patch. The
#                            make_patch_matrix() is defined below and is called by the
#                            initialize_algorithm() method of the PatchMatch class. For
#                            your purposes, you may assume that source_patches[i,j,c,:]
#                            gives you the list of intensities for color channel c of
#                            all pixels in the patch centered at pixel [i,j]. Note that patches
#                            that go beyond the image border will contain NaN values for
#                            all patch pixels that fall outside the source image.
#     - target_patches:      The matrix holding the patches of the target image.
#     - f:                   The current nearest-neighbour field
#     - alpha, w:            Algorithm parameters, as explained in Section 3 and Eq.(1)
#     - propagation_enabled: If true, propagation should be performed.
#                            Use this flag for debugging purposes, to see how your
#                            algorithm performs with (or without) this step
#     - random_enabled:      If true, random search should be performed.
#                            Use this flag for debugging purposes, to see how your
#                            algorithm performs with (or without) this step.
#     - odd_iteration:       True if and only if this is an odd-numbered iteration.
#                            As explained in Section 3.2 of the paper, the algorithm
#                            behaves differently in odd and even iterations and this
#                            parameter controls this behavior.
#     - best_D:              And NxM matrix whose element [i,j] is the similarity score between
#                            patch [i,j] in the source and its best-matching patch in the
#                            target. Use this matrix to check if you have found a better
#                            match to [i,j] in the current PatchMatch iteration
#     - global_vars:         (optional) if you want your function to use any global variables,
#                            you can pass them to/from your function using this argument

# Return arguments:
#     - new_f:               The updated NNF
#     - best_D:              The updated similarity scores for the best-matching patches in the
#                            target
#     - global_vars:         (optional) if you want your function to use any global variables,
#                            return them in this argument and they will be stored in the
#                            PatchMatch data structure


def propagation_and_random_search(source_patches, target_patches,
                                  f, alpha, w,
                                  propagation_enabled, random_enabled,
                                  odd_iteration, best_D=None,
                                  global_vars=None
                                ):
    new_f = f.copy()
    #############################################
    ###  PLACE YOUR CODE BETWEEN THESE LINES  ###
    #############################################
    def _similarity(a, b):
        return -np.linalg.norm(a - b)
    source_patches[np.isnan(source_patches)] = 0
    target_patches[np.isnan(target_patches)] = 0
    if not random_enabled:
        import math
        random_len = int(np.ceil(math.log(1.0 / w, alpha)))
        alphalist = []
        for i in range(random_len):
            alphalist.append(np.power(alpha, i))
        alphalist = np.array(alphalist).reshape(random_len, 1)

    if best_D is None:
        best_D = np.zeros((new_f.shape[0], new_f.shape[1]))
        for i in range(source_patches.shape[0]):
            for j in range(source_patches.shape[1]):
                best_D[i, j] = _similarity(source_patches[i,j], target_patches[i + new_f[i,j,0], j + new_f[i,j,1]])

    if odd_iteration:
        offset = 1
        start0 = 0
        end0 = best_D.shape[0]
        start1 = 0
        end1 = best_D.shape[1]
    else:
        offset = -1 
        end0 = -1
        start0 = best_D.shape[0] - 1
        end1 = -1
        start1 = best_D.shape[1] - 1

    for i in range(start0, end0, offset):
        for j in range(start1, end1, offset):
            if not propagation_enabled:
                current = np.array([i, j])
                if i - offset >= 0 and i - offset < best_D.shape[0]:
                    new_coord = current + new_f[i - offset, j]
                    if new_coord[0] < 0 or new_coord[0] >= best_D.shape[0]:
                        continue
                    if new_coord[1] < 0 or new_coord[1] >= best_D.shape[1]:
                        continue
                    new_score = _similarity(source_patches[i, j], target_patches[new_coord[0], new_coord[1]])
                    if new_score > best_D[i, j]:
                        best_D[i, j] = new_score
                        new_f[i, j] = new_f[i - offset, j]

                if j - offset >= 0 and j - offset < best_D.shape[1]:
                    new_coord = current + new_f[i, j - offset]
                    if new_coord[0] < 0 or new_coord[0] >= best_D.shape[0]:
                        continue
                    if new_coord[1] < 0 or new_coord[1] >= best_D.shape[1]:
                        continue
                    new_score = _similarity(source_patches[i, j], target_patches[new_coord[0], new_coord[1]])
                    if new_score > best_D[i, j]:
                        best_D[i, j] = new_score
                        new_f[i, j] = new_f[i, j - offset]

            if not random_enabled:
                M, N = best_D.shape
                R = np.random.uniform(-1, 1, (random_len, 2))
                u = (np.tile(new_f[i,j], (random_len, 1)) + (w * alphalist * R)).astype(int)
                ux = u[:,0] + i
                uy = u[:,1] + j
                ux[ux >= M] = M - 1
                ux[ux < 0] = 0
                uy[uy >= N] = N - 1
                uy[uy < 0] = 0
                target_chosen = target_patches[ux, uy]
                source_matrix = np.tile(source_patches[i,j], (random_len, 1, 1))
                scores = -np.linalg.norm(source_matrix - target_chosen, axis = (1, 2))
                scores[random_len - 1] = best_D[i, j]
                ux[random_len - 1] = new_f[i,j,0] + i
                uy[random_len - 1] = new_f[i,j,1] + j
                idx = np.argmax(scores)
                best_D[i, j] = scores[idx]
                new_f[i, j, 0] = ux[idx] - i
                new_f[i, j, 1] = uy[idx] - j
    #############################################

    return new_f, best_D, global_vars


# This function uses a computed NNF to reconstruct the source image
# using pixels from the target image. The function takes two input
# arguments
#     - target: the target image that was used as input to PatchMatch
#     - f:      the nearest-neighbor field the algorithm computed
# and should return a reconstruction of the source image:
#     - rec_source: an openCV image that has the same shape as the source image
#
# To reconstruct the source, the function copies to pixel (x,y) of the source
# the color of pixel (x,y)+f(x,y) of the target.
#
# The goal of this routine is to demonstrate the quality of the computed NNF f.
# Specifically, if patch (x,y)+f(x,y) in the target image is indeed very similar
# to patch (x,y) in the source, then copying the color of target pixel (x,y)+f(x,y)
# to the source pixel (x,y) should not change the source image appreciably.
# If the NNF is not very high quality, however, the reconstruction of source image
# will not be very good.
#
# You should use matrix/vector operations to avoid looping over pixels,
# as this would be very inefficient

def reconstruct_source_from_target(target, f):
    rec_source = None

    #############################################
    ###  PLACE YOUR CODE BETWEEN THESE LINES  ###
    #############################################
    coord = make_coordinates_matrix(target.shape)
    rec_source = target[coord[:, :, 0] + f[:, :, 0], coord[:, :, 1] + f[:, :, 1]]

    #############################################

    return rec_source


# This function takes an NxM image with C color channels and a patch size P
# and returns a matrix of size NxMxCxP^2 that contains, for each pixel [i,j] in
# in the image, the pixels in the patch centered at [i,j].
#
# You should study this function very carefully to understand precisely
# how pixel data are organized, and how patches that extend beyond
# the image border are handled.


def make_patch_matrix(im, patch_size):
    phalf = patch_size // 2
    # create an image that is padded with patch_size/2 pixels on all sides
    # whose values are NaN outside the original image
    padded_shape = im.shape[0] + patch_size - 1, im.shape[1] + patch_size - 1, im.shape[2]
    padded_im = np.zeros(padded_shape) * np.NaN
    padded_im[phalf:(im.shape[0] + phalf), phalf:(im.shape[1] + phalf), :] = im

    # Now create the matrix that will hold the vectorized patch of each pixel. If the
    # original image had NxM pixels, this matrix will have NxMx(patch_size*patch_size)
    # pixels
    patch_matrix_shape = im.shape[0], im.shape[1], im.shape[2], patch_size ** 2
    patch_matrix = np.zeros(patch_matrix_shape) * np.NaN
    for i in range(patch_size):
        for j in range(patch_size):
            patch_matrix[:, :, :, i * patch_size + j] = padded_im[i:(i + im.shape[0]), j:(j + im.shape[1]), :]

    return patch_matrix


# Generate a matrix g of size (im_shape[0] x im_shape[1] x 2)
# such that g(y,x) = [y,x]
#
# Step is an optional argument used to create a matrix that is step times
# smaller than the full image in each dimension
#
# Pay attention to this function as it shows how to perform these types
# of operations in a vectorized manner, without resorting to loops


def make_coordinates_matrix(im_shape, step=1):
    """
    Return a matrix of size (im_shape[0] x im_shape[1] x 2) such that g(x,y)=[y,x]
    """
    range_x = np.arange(0, im_shape[1], step)
    range_y = np.arange(0, im_shape[0], step)
    axis_x = np.repeat(range_x[np.newaxis, ...], len(range_y), axis=0)
    axis_y = np.repeat(range_y[..., np.newaxis], len(range_x), axis=1)

    return np.dstack((axis_y, axis_x))
